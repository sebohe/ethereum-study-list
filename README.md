Just some links I've accumlated...

https://github.com/ethereum/remix-ide

https://truffleframework.com/ (truffle and ganache)


You will then probably want to use a library:

https://github.com/OpenZeppelin/openzeppelin-solidity/

https://github.com/dapphub/dappsys


Tutorials:

https://blog.zeppelin.solutions/the-hitchhikers-guide-to-smart-contracts-in-ethereum-848f08001f05

https://ethernaut.zeppelin.solutions/

https://cryptozombies.io/


Code examples:

https://github.com/mattdf/payment-channel

https://github.com/bokkypoobah/BokkyPooBahsDateTimeLibrary

https://github.com/trailofbits/not-so-smart-contracts

https://github.com/SpankChain/general-state-channels

https://github.com/bohendo/ck-mill

https://github.com/clesaege/HackSmartContract


Extra stuff below 
———————————————————
general lists:

http://awesome-ethereum.com/#tutorials

Really nice list: https://github.com/ConsenSysLabs/ethereum-developer-tools-list

https://github.com/fravoll/solidity-patterns

https://github.com/PhABC/ethereum-token-standards-list

https://github.com/Scanate/EthList


Code analyzers:

https://github.com/pirapira/awesome-ethereum-virtual-machine#code-analyzers


Theory improvements:

https://github.com/trailofbits/evm-opcodes

https://github.com/fravoll/solidity-patterns

https://medium.com/@blockchain101/solidity-bytecode-and-opcode-basics-672e9b1a88c2

https://media.consensys.net/when-to-use-revert-assert-and-require-in-solidity-61fb2c0e5a57?gi=cb3b396a0b4d
